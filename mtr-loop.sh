#!/bin/bash
# 
#   Name:         mtr-loop.sh
#   Desc:         Run mtr in an infinite loop.
#   URL:          https://gitlab.com/lyndell/local-mtr-loop
#                
#   Author:       Lyndell Rottmann
#   WWW:          http://Lyndell.US
#   Copyright (c) 2020 Lyndell Rottmann
#   

# Debug mode on/off: (default off)
set +x

if [ $1 ]; then
  TESTIP=${1}
else
  echo "USAGE: Enter an IP to run MTR to."
  exit 128
fi

function quit ()
{
  echo -e "\n*** Exiting by user keystroke ***\n"
  exit $?
}
 
# trap keyboard interrupt (control-c)
trap quit SIGINT

echo -e "\n*** Press ctrol+c to quit. ***\n\n"

while true
do
  date -u
  echo ""
  mtr -r -c 100 ${TESTIP}
  echo ""
done

